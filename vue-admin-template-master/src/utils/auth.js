import Cookies from 'js-cookie'
import store from '@/store'

const TokenKey = 'vue_admin_template_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function authLogin(self) {
  var token = getToken()
  if (!token) {
    self.$confirm('请先登录', '提示', {
      confirmButtonText: '去登录',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(() => {
      var re_path = self.$route.path
      self.$router.push({path: `/login?redirect=${re_path}`})
    }).catch(() => {
      
    });
  }else{
    return true
  }
}

export function authRealNameAndPWS(self,authType) {
  var authResult =  store.getters.accountStatus[authType]
  var msg = authType == 'realNameAuth' ? '您还未实名验证':'您还未设置支付密码'
  if (!authResult) {
    self.$confirm(msg, {
      confirmButtonText: '去设置',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(() => {
      self.$router.push({path: '/account/safe'})
    }).catch(() => {
      self.$router.go(-1)
    });
  }else{
    return true
  }
}


