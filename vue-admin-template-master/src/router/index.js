import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/loanapply',
    component: () => import('@/views/loanapply/index'),
    hidden: true
  },

  {
    path: '/invest',
    component: () => import('@/views/invest/index'),
    hidden: true
  },

  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true
  },

  {
    path: '/',
    component: () => import('@/views/home/index')
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/dashboard',
    component: Layout,
    children: [{
      path: '',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '个人主页', icon: 'el-icon-user-solid' }
    }]
  },

  {
    path: '/product',
    component: Layout,
    children: [{
      path: '',
      name: 'Product',
      component: () => import('@/views/productManage/index'),
      meta: { title: '产品管理', icon: 'el-icon-suitcase' }
    }]
  },

  {
    path: '/award',
    component: Layout,
    children: [{
      path: '',
      name: 'Award',
      component: () => import('@/views/award/index'),
      meta: { title: '奖励管理', icon: 'el-icon-sugar' }
    }]
  },

  {
    path: '/assetManage',
    component: Layout,
    redirect: '/assetManage/collection',
    meta: { title: '资产管理', icon: 'el-icon-s-finance' },
    children: [{
      path: 'collection',
      name: 'Collection',
      component: () => import('@/views/assetManage/collection'),
      meta: { title: '资产统计', icon: 'el-icon-pie-chart' }
    },
    {
      path: 'recharge',
      name: 'Recharge',
      component: () => import('@/views/assetManage/recharge'),
      meta: { title: '充值', icon: 'el-icon-circle-plus-outline' }
    },
    {
      path: 'extract',
      name: 'Extract',
      component: () => import('@/views/assetManage/extract'),
      meta: { title: '提现', icon: 'el-icon-remove-outline' }
    }]
  },

  {
    path: '/investManage',
    component: Layout,
    redirect: '/investManage/approve',
    meta: { title: '投资管理', icon: 'el-icon-shopping-bag-2' },
    children: [{
      path: 'myInvest',
      name: 'MyInvest',
      component: () => import('@/views/investManage/myInvest'),
      meta: { title: '我的投资', icon: 'el-icon-tickets' }
    },
    {
      path: 'dealRecord',
      name: 'DealRecord',
      component: () => import('@/views/investManage/dealRecord'),
      meta: { title: '交易记录', icon: 'el-icon-sort' }
    },
    {
      path: 'exceptedReturn',
      name: 'ExceptedReturn',
      component: () => import('@/views/investManage/exceptedReturn'),
      meta: { title: '预期收益', icon: 'el-icon-money' }
    }]
  },

  {
    path: '/loanManage',
    component: Layout,
    redirect: '/loanManage/approve',
    meta: { title: '借款管理', icon: 'el-icon-s-fold' },
    children: [{
      path: 'approve',
      name: 'Approve',
      component: () => import('@/views/loanManage/loanApprove'),
      meta: { title: '借款审批', icon: 'el-icon-open' }
    },
    {
      path: 'myLoan',
      name: 'MyLoan',
      component: () => import('@/views/loanManage/myLoan'),
      meta: { title: '我的借款', icon: 'el-icon-tickets' }
    }]
  },

  {
    path: '/match',
    component: Layout,
    redirect: '/match/funds',
    meta: { title: '撮合管理', icon: 'el-icon-connection' },
    children: [{
      path: 'funds',
      name: 'Funds',
      component: () => import('@/views/match/funds'),
      meta: { title: '资金队列', icon: 'el-icon-guide' }
    },
    {
      path: 'matchedResult',
      name: 'MatchedResult',
      component: () => import('@/views/match/matchedResult'),
      meta: { title: '匹配结果', icon: 'el-icon-tickets' }
    }]
  },

  {
    path: '/debt',
    component: Layout,
    children: [{
      path: '',
      name: 'Debt',
      component: () => import('@/views/debt/index'),
      meta: { title: '债权管理', icon: 'el-icon-s-unfold' }
    }]
  },

  {
    path: '/message',
    component: Layout,
    redirect: '/message/card',
    meta: { title: '消息管理', icon: 'el-icon-message' },
    children: [{
      path: 'send',
      name: 'Send',
      component: () => import('@/views/message/send'),
      meta: { title: '消息发送', icon: 'el-icon-chat-dot-round' }
    }, {
      path: 'myMsg',
      name: 'MyMsg',
      component: () => import('@/views/message/myMsg'),
      meta: { title: '我的消息', icon: 'el-icon-chat-line-round' }
    }]
  },

  {
    path: '/account',
    component: Layout,
    redirect: '/account/card',
    meta: { title: '账号设置', icon: 'el-icon-s-tools' },
    children: [{
      path: 'safe',
      name: 'Safe',
      component: () => import('@/views/account/safe'),
      meta: { title: '安全设置', icon: 'el-icon-user' }
    }, {
      path: 'card',
      name: 'Card',
      component: () => import('@/views/account/card'),
      meta: { title: '银行卡', icon: 'el-icon-bank-card' }
    }]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
