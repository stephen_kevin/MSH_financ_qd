import request from '@/utils/request'

// 登录
export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

// 获取用户信息
export function getInfo(token) {
  return request({
    url: '/user/userInfo',
    method: 'get',
    params: { token }
  })
}

// 退出登录
export function logout() {
  return request({
    url: '/user/loginOut',
    method: 'post'
  })
}

// 账号注册
export function register(data) {
  return request({
    url: '/user/user-register',
    method: 'post',
    data:data
  })
}

// 获取验证码
export function getSmsCode(data) {
  return request({
    url: '/user/sms',
    method: 'get',
    params:data
  })
}

// 验证手机号是否已存在
export function checkPhone(data) {
  return request({
    url: '/user/isExist',
    method: 'post',
    data:data
  })
}

// 实名认证
export function realNameAuth(data) {
  return request({
    url: '/user/realName',
    method: 'post',
    data:data
  })
}

// 生成邀请码
export function createInviteCode() {
  return request({
    url: '/user/invite',
    method: 'post',
  })
}

// 获取邀请列表
export function getInviteList() {
  return request({
    url: '/user/invite',
    method: 'get',
  })
}
