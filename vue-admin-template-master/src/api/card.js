import request from '@/utils/request'

// 获取银行卡列表
export function getBankCard() {
  return request({
    url: '/card/card',
    method: 'get',
  })
}

// 添加银行卡
export function addBankCard(data) {
    return request({
      url: '/card/card',
      method: 'post',
      data:data
    })
  }

  // 删除银行卡
export function delBankCard(data) {
    return request({
      url: '/card/card',
      method: 'delete',
      data:data
    })
  }