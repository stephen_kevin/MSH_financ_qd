import request from '@/utils/request'

// 借款申请
export function loanApply(data) {
  return request({
    url: '/transaction/loanApply',
    method: 'post',
    data
  })
}

// 获取借款申请列表
export function getLoanApplyList(data) {
  return request({
    url: '/transaction/loanApply',
    method: 'get',
    params:data
  })
}

// 审批借款(更新借款申请的状态)
export function updateLoanApplystate(data) {
  return request({
    url: '/transaction/loanApply',
    method: 'put',
    data:data
  })
}

// 获取我的借款申请
export function getMyloanList(data) {
  return request({
    url: '/transaction/myLoan',
    method: 'get',
    params:data
  })
}

// 获取债权列表
export function getDebtList(data) {
  return request({
    url: '/transaction/debt',
    method: 'get',
    params:data
  })
}