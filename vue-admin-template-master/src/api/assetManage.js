import request from '@/utils/request'

// 充值
export function recharge(data) {
    return request({
      url: '/account/recharge',
      method: 'post',
      data:data
    })
  }

// 提现
export function extract(data) {
  return request({
    url: '/account/extract',
    method: 'post',
    data:data
  })
}

