import request from '@/utils/request'

// 获取投资产品列表
export function getProductList() {
  return request({
    url: '/product/proList',
    method: 'get',
  })
}

// 获取投资产品利率
export function getProductRate(data) {
    return request({
      url: '/product/proRateList',
      method: 'get',
      params:data
    })
  }

  // 购买理财产品
export function buyProduct(data) {
    return request({
      url: '/transaction/invest',
      method: 'post',
      data:data
    })
  }

// 获取我的投资记录
export function getInvestRecord(data) {
  return request({
    url: '/transaction/invest_record',
    method: 'get',
    params:data
  })
}