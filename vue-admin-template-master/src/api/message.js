import request from '@/utils/request'

// 消息发送
export function msgSend(data) {
  return request({
    url: '/notify/message',
    method: 'post',
    data:data
  })
}

// 获取消息列表
export function getMsg(data) {
  return request({
    url: '/notify/message',
    method: 'get',
    params:data
  })
}

// 更新消息已读状态
export function checkMsg(data) {
  return request({
    url: '/notify/message',
    method: 'put',
    data:data
  })
}

// 删除消息
export function delMsg(data) {
  return request({
    url: '/notify/message',
    method: 'delete',
    data:data
  })
}

