import request from '@/utils/request'

// 获取资金队列
export function getFunds(data) {
  return request({
    url: '/transaction/fundsNotMatched',
    method: 'get',
    params:data
  })
}

// 撮合匹配
export function matchUp(data) {
  return request({
    url: '/transaction/matchUp',
    method: 'post',
  })
}


// 获取匹配结果
export function getMatchedResult(data) {
  return request({
    url: '/transaction/matchedResult',
    method: 'get',
    params:data
  })
}

// 获取还款计划
export function getRepayPlan(data) {
  return request({
    url: '/transaction/RepayPlan',
    method: 'get',
    params:data
  })
}


// 还款
export function rePay(data) {
  return request({
    url: '/transaction/RepayPlan',
    method: 'post',
    data:data
  })
}

// 获取交易记录
export function getDealRecord(data) {
  return request({
    url: '/transaction/deal_record',
    method: 'get',
    params:data
  })
}

// 提取收益
export function getIncome(data) {
  return request({
    url: '/transaction/getIncome',
    method: 'post',
    data:data
  })
}

// 获取预期收益
export function getExceptedReturn(data) {
  return request({
    url: '/transaction/exceptedReturn',
    method: 'get',
    params:data
  })
}

