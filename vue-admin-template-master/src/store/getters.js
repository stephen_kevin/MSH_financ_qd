const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  invite_code: state => state.user.invite_code,
  accountStatus: state => state.user.accountStatus,
  accountInfo: state => state.user.accountInfo,
  productList: state => state.invest.productList,
  bankCardList: state => state.card.bankCardList,
}
export default getters
