import { login, logout, getInfo, register, getSmsCode, checkPhone, realNameAuth, createInviteCode, getInviteList } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    accountStatus: null, // 用户账号信息
    accountInfo: null,  // 用户账户信息（资产相关）
    roles: [],  // 权限组
    invite_code: ''  // 注册邀请码
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, data) => {
    state.roles = data
  },
  SET_INVITECODE: (state, data) => {
    state.invite_code = data
  },
  // 保存用户的相关认证状态到state
  SET_ACCOUNTSTATUS: (state, data) => {
    state.accountStatus = data
  },
  SET_ACCOUNTINFO: (state, data) => {
    state.accountInfo = data
  },
  SET_REALNAME: (state, data) => {
    // 更改实名认证的状态
    state.accountStatus.realNameAuth = 1
  },
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info 获取用户信息
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { name, roles, userInfoData, accountInfo } = data
        console.log('获取的用户信息', data)
        commit('SET_NAME', name)
        const avatar = userInfoData.avatar || 'baseAvatar.jpg'
        commit('SET_AVATAR', avatar)
        commit('SET_INVITECODE', userInfoData.invite_code)
        commit('SET_ROLES', roles)
        commit('SET_ACCOUNTSTATUS', userInfoData)
        commit('SET_ACCOUNTINFO', accountInfo)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 注册
  register({ commit, state }, data) {
    return new Promise((resolve, reject) => {
      register(data).then((res) => {
        const { data } = res
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 获取验证码
  getSmsCode({ commit, state }, phoneNumber) {
    return new Promise((resolve, reject) => {
      getSmsCode(phoneNumber).then((res) => {
        const { data } = res
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 判断手机号是否存在
  checkPhone({ commit, state }, phoneNumber) {
    return new Promise((resolve, reject) => {
      checkPhone(phoneNumber).then((res) => {
        const { data } = res
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 实名认证
  realNameAuth({ commit, state }, data) {
    return new Promise((resolve, reject) => {
      realNameAuth(data).then((res) => {
        commit('SET_REALNAME')
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 生成邀请码
  createInviteCode({ commit, state }, data) {
    return new Promise((resolve, reject) => {
      createInviteCode(data).then((res) => {
        commit('SET_INVITECODE', res.data.invite_code)
        resolve(res.data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 获取邀请列表
  getInviteList({ commit, state }, data) {
    return new Promise((resolve, reject) => {
      getInviteList(data).then((res) => {
        resolve(res.data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

