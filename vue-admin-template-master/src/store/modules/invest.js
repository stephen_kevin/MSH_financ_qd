import { getProductList, getProductRate, buyProduct, getInvestRecord  } from '@/api/invest'

const state = {
    productList:[]
}

const mutations ={
    SET_PRODUCTLIST:(state, list) =>{
        state.productList = list
    }
}

const actions = {
    // 获取投资产品列表
    getProductList({ commit }, ) {
      return new Promise((resolve, reject) => {
        getProductList().then(response => {
        commit('SET_PRODUCTLIST', response.data.data)
          resolve(response.data.data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取投资产品利率
    getProductRate({ commit }, data) {
        return new Promise((resolve, reject) => {
            getProductRate(data).then(response => {
            resolve(response.data.data)
          }).catch(error => {
            reject(error)
          })
        })
      },

      // 购买理财产品
      buyProduct({ commit }, data) {
        return new Promise((resolve, reject) => {
            buyProduct(data).then(response => {
            resolve()
          }).catch(error => {
            reject(error)
          })
        })
      },

      // 获取我的投资记录
      getInvestRecord({ commit }, data) {
      return new Promise((resolve, reject) => {
        getInvestRecord(data).then(response => {
          resolve(response.data.data)
        }).catch(error => {
          reject(error)
        })
      })
    },
}

export default {
    namespaced: true,
    actions,
    state,
    mutations
  }