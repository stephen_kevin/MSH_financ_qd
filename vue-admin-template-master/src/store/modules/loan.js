import { loanApply, getLoanApplyList, updateLoanApplystate, getMyloanList, getDebtList } from '@/api/loan'

const actions = {
    // 借款申请
    loanApply({ commit }, data) {
      return new Promise((resolve, reject) => {
        loanApply(data).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取借款申请列表
    getLoanApplyList({ commit }, data) {
      return new Promise((resolve, reject) => {
        getLoanApplyList(data).then(response => {
          resolve(response.data.data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取我的借款申请列表
    getMyloanList({ commit }, data) {
      return new Promise((resolve, reject) => {
        getMyloanList(data).then(response => {
          resolve(response.data.data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 审批借款(更新借款申请的状态)
    updateLoanApplystate({ commit }, data) {
      return new Promise((resolve, reject) => {
        updateLoanApplystate(data).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取债权列表
    getDebtList({ commit }, data) {
      return new Promise((resolve, reject) => {
        getDebtList(data).then(response => {
          resolve(response.data.data)
        }).catch(error => {
          reject(error)
        })
      })
    },
}

export default {
    namespaced: true,
    actions
  }