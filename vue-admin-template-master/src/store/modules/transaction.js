import { matchUp, getFunds, getMatchedResult, getRepayPlan, rePay, getDealRecord, getExceptedReturn, getIncome } from '@/api/transaction'

const actions = {

    // 获取资金队列
    getFunds({ commit }, data) {
        return new Promise((resolve, reject) => {
            getFunds(data).then(response => {
                resolve(response.data.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 获取匹配结果
    getMatchedResult({ commit }, data) {
        return new Promise((resolve, reject) => {
            getMatchedResult(data).then(response => {
                resolve(response.data.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 撮合匹配
    matchUp({ commit }, data) {
        return new Promise((resolve, reject) => {
            matchUp(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 获取还款计划
    getRepayPlan({ commit }, data) {
        return new Promise((resolve, reject) => {
            getRepayPlan(data).then(response => {
                resolve(response.data.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 还款
    rePay({ commit }, data) {
        return new Promise((resolve, reject) => {
            rePay(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 获取交易记录
    getDealRecord({ commit }, data) {
        return new Promise((resolve, reject) => {
            getDealRecord(data).then(response => {
                resolve(response.data.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 提取收益
    getIncome({ commit }, data) {
        return new Promise((resolve, reject) => {
            getIncome(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 获取预期收益
    getExceptedReturn({ commit }, data) {
        return new Promise((resolve, reject) => {
            getExceptedReturn(data).then(response => {
                resolve(response.data.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

}

export default {
    namespaced: true,
    actions,
}