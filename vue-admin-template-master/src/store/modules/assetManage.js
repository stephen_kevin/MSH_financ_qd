import { recharge, extract} from '@/api/assetManage'


const actions = {

    // 充值
    recharge({ commit }, data) {
        return new Promise((resolve, reject) => {
            recharge(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 提现
    extract({ commit }, data) {
        return new Promise((resolve, reject) => {
            extract(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    
}

export default {
    namespaced: true,
    actions,
}