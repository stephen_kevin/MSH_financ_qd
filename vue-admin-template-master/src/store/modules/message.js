import { msgSend, getMsg, checkMsg, delMsg } from '@/api/message'

const actions = {

    // 发送消息
    msgSend({ commit }, data) {
        return new Promise((resolve, reject) => {
            msgSend(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },
    // 获取消息列表
    getMsg({ commit }, data) {
        return new Promise((resolve, reject) => {
            getMsg(data).then(response => {
                resolve(response.data.data)
            }).catch(error => {
                reject(error)
            })
        })
    },
    // 更新消息已读状态
    checkMsg({ commit }, data) {
        return new Promise((resolve, reject) => {
            checkMsg(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },
    // 删除消息
    delMsg({ commit }, data) {
        return new Promise((resolve, reject) => {
            delMsg(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

}

export default {
    namespaced: true,
    actions,
}