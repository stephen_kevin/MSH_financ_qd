import { getBankCard, addBankCard, delBankCard } from '@/api/card'

const state = {
    bankCardList: []
}

const mutations = {
    SET_CARDLIST: (state, list) => {
        state.bankCardList = list
    }
}

const actions = {
    // 获取银行卡列表
    getBankCard({ commit },) {
        return new Promise((resolve, reject) => {
            getBankCard().then(response => {
                commit('SET_CARDLIST', response.data)
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 添加银行卡
    addBankCard({ commit }, data) {
        return new Promise((resolve, reject) => {
            addBankCard(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 删除银行卡
    delBankCard({ commit }, data) {
        return new Promise((resolve, reject) => {
            delBankCard(data).then(response => {
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    namespaced: true,
    actions,
    state,
    mutations
}