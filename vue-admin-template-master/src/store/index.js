import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import loan from './modules/loan'
import invest from './modules/invest'
import card from './modules/card'
import assetManage from './modules/assetManage'
import message from './modules/message'
import transaction from './modules/transaction'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    loan,
    invest,
    card,
    assetManage,
    message,
    transaction
  },
  getters
})

export default store
