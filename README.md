# 马上有钱金融平台前端vue代码

#### 介绍

马上有钱前端代码，采用vue+echarts数据可视化，并且使用element——ul的组件设计了本次网站，，后台使用python语言的falsk的框架完成的网站系统
数据库采用的事mysql数据库，通过和本地的连接可以完成本代码的功能

#### 软件架构
- vue3
- element_ui组件
- echarts数据可视化
- vue-admin后台管理布置

#### 安装教程
1.  需要使用淘宝的依赖
2.  npm install echarts
3.  npm install element-ui

#### 使用说明
1.  可以使用vscode启动项目
2.  可以使用其他工具软件启动项目
3.  使用npm run dev或者npm run server启动项目，要在终端运行

#### 参与贡献
1.  Fork 本仓库
2.  新建 master 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
7.  flask框架